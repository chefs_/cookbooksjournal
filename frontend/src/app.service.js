import axios from 'axios'

axios.interceptors.request.use(function (config) {
  const token = window.localStorage.getItem('token')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, function (error) {
  debugger // eslint-disable-line
  const originalRequest = error.config
  if (error.config.status === 401 && !originalRequest._retry) {
    originalRequest._retry = true
    const refreshToken = window.localStorage.getItem('refreshToken')
    return axios.post('http://localhost:8000/auth/refresh', { refreshToken })
      .then(({data}) => {
        window.localStorage.setItem('token', data.access_token)
        window.localStorage.setItem('refreshToken', data.refresh_token)
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token
        originalRequest.headers['Authorization'] = 'Bearer ' + data.access_token
        return axios(originalRequest)
      })
  }
  return Promise.reject(error)
})

const appService = {
  /* getPosts (categoryId) {
    return new Promise((resolve) => {
      axios.get('/wp-json/wp/v2/posts?categories=${categoryId}&per_page=6')
        .then(response => {
          resolve(response.data)
        })
    })
  }, */
  getCategories () {
    return new Promise((resolve) => {
      axios.get('http://localhost:8081/category/find/')
        .then(response => {
          resolve(response.data)
        })
    })
  },
  getUserDetails () {
    return new Promise((resolve) => {
      axios.get('http://localhost:8081/user/find/1')
        .then(response => {
          resolve(response.data)
        })
    })
  },
  getRecipes () {
    return new Promise((resolve) => {
      axios.get('http://localhost:8081/recipe/find/')
        .then(response => {
          resolve(response.data)
        })
    })
  },
  postNewCategory (category, userId) {
    axios.post('http://localhost:8081/category/save', {
      'name': category,
      'userId': userId
    })
      .then(function (response) {
        console.log(response + ':> new category added')
      })
      .catch(function (error) {
        console.log(error + ':(')
      })
  },
  postNewUser (firstName, lastName, username, birthday, gender, password, role, email) {
    axios.post('http://localhost:8081/user/save', {
      'mail': email,
      'username': username,
      'password': password,
      'first_name': firstName,
      'last_name': lastName,
      'birthday': birthday,
      'gender': gender,
      'role_id': role
    })
      .then(function (response) {
        console.log(response + ':> user added')
      })
      .catch(function (error) {
        console.log(error + ':(')
      })
  },
  postNewRecipe (name, description, date, prepTime, cookTime, serves, notes, instructions, ingredients, category, userId, state, rating, ingredientId) {
    axios.post('http://localhost:8081/recipe/save', {
      'name': name,
      'description': description,
      'date': date,
      'prepTime': prepTime,
      'cookTime': cookTime,
      'serves': serves,
      'notes': notes,
      'instructions': instructions,
      'ingredients': ingredients,
      'category': category,
      'userId': userId,
      'ingredientId': ingredientId,
      'star': rating,
      'state': state

    })
      .then(function (response) {
        console.log(response + ':> new recipe added')
      })
      .catch(function (error) {
        console.log(error + ':(')
      })
  },
  deleteRecipe (idRecipe) {
    axios.delete('http://localhost:8081/recipe/delete/' + idRecipe)
      .then(function (response) {
        console.log(response + ' at delete :>')
      })
      .catch(function (error) {
        console.log(error + ' at delete :(')
      })
  },
  login (credentials) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: 'http://localhost:8081/oauth/token',
        headers: {
          accept: 'application/json',
          'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'
        },
        params: {
          grant_type: 'password',
          username: credentials.username,
          password: credentials.password
        },
        auth: {
          username: 'webapp',
          password: 'secret'
        },
        withCredentials: true
      })
        .then(response => {
          resolve(response.data)
        }).catch(response => {
          reject(response.status)
        })
    })
  }
}

export default appService
