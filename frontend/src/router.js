import Vue from 'vue'
import store from './vuex/index.js'
import VueRouter from 'vue-router'
// import Category from './theme/Category.vue'
import Login from './theme/Login.vue'
import NotFound from './theme/NotFound.vue'
import Registration from './theme/Registration.vue'
import DisplayRecipes from './theme/DisplayRecipes.vue'
import NewRecipe from './theme/NewRecipe.vue'
import ResetPassword from './theme/ResetPassword.vue'

// const Category = ()=>System.import('./theme/Category.vue')
// const Login = ()=>System.import('./theme/Login.vue')
// const NotFound = ()=>System.import('./theme/Notfound.vue')

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'is-active',
  scrollBehavior: (to, from, savedPosition) => ({ y: 0 }),
  routes: [
    // { path: '/category/:id', name: 'category', component: Category },
    { path: '/login', component: Login },
    { path: '/logout', component: Login, meta: { auth: true } },
    { path: '/reset', component: ResetPassword },
    { path: '*', component: NotFound },
    { path: '/', redirect: '/login' },
    { path: '/registration', component: Registration, meta: { auth: true } },
    { path: '/recipes', component: DisplayRecipes, meta: { auth: true } },
    { path: '/create', component: NewRecipe, meta: { auth: true } }
  ]
})

router.beforeEach((to, from, next) => {
  const authRequired = to.matched.some((routes) => routes.meta.auth)
  console.log('state is ' + JSON.stringify(store.state))
  const authed = store.state
  if (authRequired && !authed) {
    next('/login')
  } else {
    next()
  }
})

export default router
