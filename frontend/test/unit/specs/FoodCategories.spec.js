import 'es6-promise/auto'
import Vue from 'vue'
import FoodCategories from '../../../src/theme/FoodCategories.vue'

describe('FoodCategories.vue', () => {
  const createComponent = () => {
    const FoodCategoriesConstructor = Vue.extend(FoodCategories)
    const comp = new FoodCategoriesConstructor({}).$mount()
    return comp
  }
  it('has a created hook', () => {
    expect(typeof FoodCategories.created).to.equal('function')
  })
  it('has a loadCategories method', () => {
    expect(typeof FoodCategories.methods.loadCategories).to.equal('function')
  })
  it('has a done method', () => {
    expect(typeof FoodCategories.methods.done).to.equal('function')
  })
  it('sets the correct default data', () => {
    expect(typeof FoodCategories.data).to.equal('function')

    const defaultData = FoodCategories.data()
    expect(defaultData.categories).to.deep.equal([])

    defaultData.categoryName = 'Dinner'
    expect(defaultData.categoryName).to.equal('Dinner')
  })
  it('correctly sets the categoryName when created', () => {
    const comp = createComponent()
    expect(comp.categoryName).to.equal('')
  })
  it('should render correct category', () => {
    const comp = createComponent()
    comp.categoryName = 'Lunch'
    comp.$nextTick(() => {
      expect(comp.categoryName).to.be.equal('Lunch')
    })
  })
})
