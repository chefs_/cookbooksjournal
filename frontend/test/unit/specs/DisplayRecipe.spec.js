import 'es6-promise/auto'
import Vue from 'vue'
import DisplayRecipes from '../../../src/theme/DisplayRecipes.vue'

describe('DisplayRecipes.vue', () => {
  const createComponent = () => {
    const DisplayRecipesConstructor = Vue.extend(DisplayRecipes)
    const comp = new DisplayRecipesConstructor({}).$mount()
    return comp
  }
  it('has a created hook', () => {
    expect(typeof DisplayRecipes.created).to.equal('function')
  })
  it('has a deleteRecipe method', () => {
    expect(typeof DisplayRecipes.methods.deleteRecipe).to.equal('function')
  })
  it('has a goToCreate method', () => {
    expect(typeof DisplayRecipes.methods.goToCreate).to.equal('function')
  })
  it('has a displayModal method', () => {
    expect(typeof DisplayRecipes.methods.displayModal).to.equal('function')
  })
  it('has a loadRecipes method', () => {
    expect(typeof DisplayRecipes.methods.loadRecipes).to.equal('function')
  })
  it('sets the correct default data', () => {
    expect(typeof DisplayRecipes.data).to.equal('function')
    const defaultData = DisplayRecipes.data()
    expect(defaultData.recipes).to.deep.equal([])
  })
  it('correctly sets the showModal when created', () => {
    const comp = createComponent()
    expect(comp.showModal).to.equal(false)
  })
})
