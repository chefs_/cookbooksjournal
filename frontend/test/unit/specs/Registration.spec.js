import 'es6-promise/auto'
import Vue from 'vue'
import Registration from '../../../src/theme/Registration.vue'

describe('Registration.vue', () => {
  const createComponent = () => {
    const RegistrationConstructor = Vue.extend(Registration)
    const comp = new RegistrationConstructor({}).$mount()
    return comp
  }
  it('has a addUser method', () => {
    expect(typeof Registration.methods.addUser).to.equal('function')
  })
  it('sets the correct default data', () => {
    expect(typeof Registration.data).to.equal('function')
  })
  it('correctly sets the user when created', () => {
    const comp = createComponent()
    expect(comp.firstname).to.equal('')
    expect(comp.lastname).to.equal('')
    expect(comp.username).to.equal('')
    expect(comp.birthday).to.equal('')
    expect(comp.picked).to.equal('')
    expect(comp.password).to.equal('')
  })
})
