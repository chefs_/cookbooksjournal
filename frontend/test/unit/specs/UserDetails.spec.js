import 'es6-promise/auto'
import UserDetails from '../../../src/theme/UserDetails.vue'

describe('UserDetails.vue', () => {
  it('has a created hook', () => {
    expect(typeof UserDetails.created).to.equal('function')
  })
  it('has a loadUserDetails method', () => {
    expect(typeof UserDetails.methods.loadUserDetails).to.equal('function')
  })
  it('sets the correct default data', () => {
    expect(typeof UserDetails.data).to.equal('function')

    const defaultData = UserDetails.data()
    expect(defaultData.details).to.deep.equal({})
  })
})
