import 'es6-promise/auto'
import Vue from 'vue'
import NewRecipe from '../../../src/theme/NewRecipe.vue'

describe('NewRecipe.vue', () => {
  const createComponent = () => {
    const NewRecipeConstructor = Vue.extend(NewRecipe)
    const comp = new NewRecipeConstructor({}).$mount()
    return comp
  }
  it('has a chosenCategory method', () => {
    expect(typeof NewRecipe.methods.chosenCategory).to.equal('function')
  })
  it('has a addRecipe method', () => {
    expect(typeof NewRecipe.methods.addRecipe).to.equal('function')
  })
  it('sets the correct default data', () => {
    expect(typeof NewRecipe.data).to.equal('function')
  })
  it('correctly sets the recipe when created', () => {
    const comp = createComponent()
    expect(comp.recipe_name).to.equal('')
    expect(comp.recipe_date).to.equal('')
    expect(comp.recipe_description).to.equal('')
    expect(comp.recipe_preptime).to.equal('')
    expect(comp.recipe_cooktime).to.equal('')
    expect(comp.recipe_serves).to.equal('')
    expect(comp.recipe_ingredients).to.equal('')
    expect(comp.recipe_instructions).to.equal('')
    expect(comp.recipe_notes).to.equal('')
    expect(comp.category).to.equal('')
  })
})
