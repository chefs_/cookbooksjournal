# Developer Notes

```stuff
git
node
npm
java
maven
docker 
docker compose
```

## Installation guide 

### Build/Run Backend
   
```bash
mvn clean install

mvn spring-boot:run
```
or 

```bash
.run_backend.sh
```

### Build/Run Frontend

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
or

```bash
.run_frontend.sh
```  

### Build/Run Database

```bash
./run_database.sh
```

### Build/Run Platform 
```bash
./run.sh
```