#!/usr/bin/env bash

# build backend application
cd ./backend/
mvn clean install

# start applications using docker
docker-compose up -d