#!/usr/bin/env bash

# start backend application
cd ./backend/
mvn clean install
mvn spring-boot:run