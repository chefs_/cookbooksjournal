CREATE TABLE CATEGORY (
  ID int NOT NULL AUTO_INCREMENT,
  NAME varchar(45) NOT NULL,
  USER_ID int NOT NULL,
  PRIMARY KEY (ID, NAME)
);