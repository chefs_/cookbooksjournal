INSERT INTO CATEGORY VALUES
    (1,'Dessert',0),
    (2,'Breakfast',0),
    (3,'Lunch', 0),
    (4,'Dinner', 0),
    (5,'Appetizers', 0),
    (6,'Entrees', 0),
    (7,'Salad', 0),
    (8,'Soup', 0),
    (9, 'Fast Food', 1),
    (10, 'Pet Food', 2);
