package com.cookbook.app.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {

    private String firstname;
    private String lastname;
    private String username;
    private Date birthday;
    private String gender;
    private String type;
    private String password;

}
