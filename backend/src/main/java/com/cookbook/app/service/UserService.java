package com.cookbook.app.service;

import com.cookbook.app.domain.User;
import com.cookbook.app.dto.UserDTO;
import com.cookbook.app.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository repository;

    private Mapper mapper = new DozerBeanMapper();

    public UserDTO find(long id) {
        Optional<User> user = repository.findById(id);
        return mapper.map(user, UserDTO.class);
    }

    public UserDTO save(UserDTO userDTO) {

        final User user = mapper.map(userDTO, User.class);
        user.setActive(1);

        User savedUser = repository.save(user);
        return mapper.map(savedUser, UserDTO.class);
    }
}

