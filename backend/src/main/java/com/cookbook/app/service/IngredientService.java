package com.cookbook.app.service;

import com.cookbook.app.domain.Ingredient;
import com.cookbook.app.repository.IngredientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class IngredientService {

    @Autowired
    private IngredientRepository repository;

    public Ingredient save(Ingredient ingredient) {
        return repository.save(ingredient);
    }

    public Ingredient find(long id) {
        final Optional<Ingredient> ingredient = repository.findById(id);
        return ingredient.get();
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

    public Iterable<Ingredient> findAll() {
        return repository.findAll();
    }

}

