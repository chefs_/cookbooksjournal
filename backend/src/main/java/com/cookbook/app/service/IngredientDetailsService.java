package com.cookbook.app.service;

import com.cookbook.app.domain.IngredientDetails;
import com.cookbook.app.repository.IngredientDetailsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class IngredientDetailsService {

    @Autowired
    private IngredientDetailsRepository repository;

    public IngredientDetails save(IngredientDetails ingredientDetails) {
        return repository.save(ingredientDetails);
    }

    public IngredientDetails find(long id) {
        final Optional<IngredientDetails> ingredientDetails = repository.findById(id);
        return ingredientDetails.get();
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

    public Iterable<IngredientDetails> findAll() {
        return repository.findAll();
    }

}