package com.cookbook.app.service;

import com.cookbook.app.domain.Recipe;
import com.cookbook.app.repository.RecipeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class RecipeService {

    @Autowired
    private RecipeRepository repository;

    public Iterable<Recipe> findAll() {
        return repository.findAll();
    }

    public Recipe find(long id) {
        final Optional<Recipe> recipe = repository.findById(id);
        return recipe.get();
    }

    public Recipe save(Recipe recipe) {
        return repository.save(recipe);
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

}
