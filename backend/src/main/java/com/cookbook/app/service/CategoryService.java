package com.cookbook.app.service;


import com.cookbook.app.domain.Category;
import com.cookbook.app.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    public Iterable<Category> findAll() {
        return repository.findAll();
    }

    public Category save(Category category) {
        return repository.save(category);
    }

    public Category find(long id) {
        final Optional<Category> category = repository.findById(id);
        return category.get();
    }

    public void delete(long id) {
        repository.deleteById(id);
    }
}
