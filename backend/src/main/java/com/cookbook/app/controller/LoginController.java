package com.cookbook.app.controller;

import com.cookbook.app.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoginController {

    @RequestMapping(value = {"/", "/login"}, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public User checkCredentials(@RequestParam(value = "username")String username,
                                 @RequestParam(value = "password")String password) {
        return null;
    }

    @RequestMapping(value="/registration", method = RequestMethod.GET)
    public User registration(){
        return null;
    }
}
