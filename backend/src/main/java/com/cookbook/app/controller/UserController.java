package com.cookbook.app.controller;

import com.cookbook.app.dto.UserDTO;
import com.cookbook.app.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService service;

    @RequestMapping(value ="/find/{id}", produces = "application/json", method = RequestMethod.GET)
    public UserDTO find(@PathVariable("id") String id) {
        return service.find(Long.valueOf(id));
    }

    @RequestMapping(value = {"/save", "/sign-up"}, produces = "application/json", method = RequestMethod.POST)
    public UserDTO save(@RequestBody UserDTO userDTO) {
        final UserDTO saved = service.save(userDTO);
        return saved;
    }
}
