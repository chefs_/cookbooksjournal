package com.cookbook.app.controller;

import com.cookbook.app.domain.Category;
import com.cookbook.app.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @CrossOrigin
    @RequestMapping(value ="/find", produces = "application/json", method = RequestMethod.GET)
    public List<Category> find() {
        return (List<Category>) service.findAll();
    }

    @RequestMapping(value ="/find/{id}", produces = "application/json", method = RequestMethod.GET)
    public Category find(@PathVariable("id") String id) {
        return service.find(Long.valueOf(id));
    }

    @RequestMapping(value = "/save", produces = "application/json", method = RequestMethod.POST)
    public Category save(@RequestBody Category category) {
        return service.save(category);
    }

    @RequestMapping(value ="/delete/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {
        service.delete(Long.valueOf(id));
    }
}
