package com.cookbook.app.controller;

import com.cookbook.app.domain.Ingredient;
import com.cookbook.app.service.IngredientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("ingredient")
public class IngredientController {
    @Autowired
    private IngredientService service;

    @RequestMapping(value ="/find", produces = "application/json", method = RequestMethod.GET)
    public List<Ingredient> find() {
        return (List<Ingredient>) service.findAll();
    }

    @RequestMapping(value ="/find/{id}", produces = "application/json", method = RequestMethod.GET)
    public Ingredient find(@PathVariable("id") String id) {
        return service.find(Long.valueOf(id));
    }

    @RequestMapping(value = "/save", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
    public Ingredient save(@RequestBody Ingredient ingredient) {
        return service.save(ingredient);
    }

    @RequestMapping(value ="/delete/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {
        service.delete(Long.valueOf(id));
    }
}
