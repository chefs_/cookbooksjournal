package com.cookbook.app.controller;

import com.cookbook.app.domain.IngredientDetails;
import com.cookbook.app.service.IngredientDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("ingredientDetails")
public class IngredientDetailsController {
    @Autowired
    private IngredientDetailsService service;
    @RequestMapping(value ="/find", produces = "application/json", method = RequestMethod.GET)
    public List<IngredientDetails> find() {
        return (List<IngredientDetails>) service.findAll();
    }

    @RequestMapping(value ="/find/{id}", produces = "application/json", method = RequestMethod.GET)
    public IngredientDetails find(@PathVariable("id") String id) {
        return service.find(Long.valueOf(id));
    }

    @RequestMapping(value = "/save", produces = "application/json", method = RequestMethod.POST)
    public IngredientDetails save(@RequestBody IngredientDetails ingredientDetails) {
        return service.save(ingredientDetails);
    }

    @RequestMapping(value ="/delete/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {
        service.delete(Long.valueOf(id));
    }
}
