package com.cookbook.app.controller;

import com.cookbook.app.domain.Recipe;
import com.cookbook.app.service.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("recipe")
public class RecipeController {

    @Autowired
    private RecipeService service;

    @RequestMapping(value="/find", produces = "application/json", method = RequestMethod.GET)
    public List<Recipe> find() {
        return (List<Recipe>) service.findAll();
    }

    @RequestMapping(value ="/find/{id}", produces = "application/json", method = RequestMethod.GET)
    public Recipe find(@PathVariable("id") String id) {
        return service.find(Long.valueOf(id));
    }

    @RequestMapping(value = "/save", produces = "application/json", method = RequestMethod.POST)
    public Recipe save(@RequestBody Recipe recipe) {
        return service.save(recipe);
    }

    @RequestMapping(value ="/delete/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {
        service.delete(Long.valueOf(id));
    }
}
