package com.cookbook.app.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="ROLE")
public class Role {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ID_USER")
    private Long user;
}
