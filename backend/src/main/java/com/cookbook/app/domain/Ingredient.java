package com.cookbook.app.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
@Table(name = "INGREDIENT")
public class Ingredient {

    @Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "USER_ID")
	private Integer userId;
}
