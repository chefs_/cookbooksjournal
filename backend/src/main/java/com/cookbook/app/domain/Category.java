package com.cookbook.app.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "CATEGORY")
public class Category {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name= "NAME")
    private String name;

    @Column(name= "USER_ID")
    private Long userId;
}
