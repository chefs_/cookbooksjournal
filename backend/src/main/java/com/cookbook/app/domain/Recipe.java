package com.cookbook.app.domain;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "RECIPE")
public class Recipe {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DATE_TIME")
    private Date date;

    @Column(name = "CATEGORY_INGR")
    private String category;

    @Column(name = "PREP_TIME")
    private String prepTime;

    @Column(name = "COOK_TIME")
    private String cookTime;

    @Column(name = "SERVES")
    private Integer serves;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "INGREDIENTS")
    private String ingredients;

    @Column(name = "INSTRUCTIONS")
    private String instructions;

    @Column(name = "ID_USER")
    private Integer userId;

    @Column(name = "INGREDIENT_ID")
    private Integer ingredientId;

    @Column(name = "STATE")
    private String state;

    @Column(name = "STAR")
    private Integer star;
}
