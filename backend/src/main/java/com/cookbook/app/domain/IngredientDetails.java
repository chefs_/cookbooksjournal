package com.cookbook.app.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
@Table(name = "INGREDIENT_DETAILS")
public class IngredientDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "RECIPE_ID")
    private Integer recipeId;

    @Column(name = "INGREDIENT_ID")
    private Long ingredientId;

    @Column(name = "MEASURE")
    private String measure;

    @Column(name = "QUANTITY")
    private String quantity;
}
