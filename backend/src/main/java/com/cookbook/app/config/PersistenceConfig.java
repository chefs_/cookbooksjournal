package com.cookbook.app.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.cookbook.app.repository")
@EntityScan({"com.cookbook.app.domain"})
public class PersistenceConfig {

}
