package com.cookbook.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private static String REALM="MY_OAUTH_REALM";

    @Autowired
    private AuthenticationManager authenticationManager;

//    @Autowired
//    private UserApprovalHandler userApprovalHandler;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private BCryptPasswordEncoder encoder;

//    @Autowired
//    @Qualifier("userDetailsService")
//    private UserDetailsService userDetailsService;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .authenticationManager(authenticationManager)
//                .userDetailsService(userDetailsService)
                .tokenStore(tokenStore);

//                .userApprovalHandler(userApprovalHandler);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
//        oauthServer
//                .tokenKeyAccess("hasAuthority('ROLE_TRUSTED_CLIENT')")
//                .checkTokenAccess("hasAuthority('ROLE_TRUSTED_CLIENT')");
        oauthServer
                .realm(REALM+"/client")
                .passwordEncoder(encoder);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("webapp")
                .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit", "client_credentials")
                .authorities("WEBAPP_CLIENT")
                .scopes("read", "write")
                .secret("$2a$04$ldADUpLRrs2INSLNHXGT5OZl6e8cWcLkJE1X2TQrovEP48hZjlg3e")
                .accessTokenValiditySeconds(120)            //Access token is only valid for 2 minutes.
                .refreshTokenValiditySeconds(600);          //Refresh token is only valid for 10 minutes.
    }
}
