package com.cookbook.app.repository;

import com.cookbook.app.domain.IngredientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientDetailsRepository extends JpaRepository<IngredientDetails, Long> {
}
