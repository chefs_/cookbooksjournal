package com.cookbook.app;

import com.cookbook.app.config.DataSourceConfig;
import com.cookbook.app.config.PersistenceConfig;
import com.cookbook.app.domain.Ingredient;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.yml")
@Import({DataSourceConfig.class, PersistenceConfig.class})
public class CookbookJournalApplicationIT {

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void save() throws Exception {

        final Ingredient ingredient = new Ingredient();
        ingredient.setName("name");


    }
}