package com.cookbook.app.service;

import com.cookbook.app.domain.Ingredient;
import com.cookbook.app.repository.IngredientRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class IngredientServiceTest {

    @InjectMocks
    private IngredientService service;

    @Mock
    private IngredientRepository repository;

    @Test
    @Ignore
    public void saveNewIngredient_once() throws Exception {

        //setup
        final Ingredient expected = new Ingredient();
        expected.setId(1L);
        expected.setName("ingredient");
        expected.setDescription("description");
        when(repository.save(any(Ingredient.class))).thenReturn(expected);

        //execute
        final Ingredient mockIngredient = mock(Ingredient.class);
        final Ingredient saved = service.save(mockIngredient);

        //verify
        assertThat(saved, is(expected));
        verify(repository, times(1)).save(mockIngredient);
    }

    @Test
    @Ignore
    public void saveNewIngredient_twice() throws Exception {

        //setup
        Ingredient expected = new Ingredient();
        expected.setId(1L);
        expected.setName("ingredient");
        expected.setDescription("instructions");
        when(repository.save(any(Ingredient.class))).thenReturn(expected);

        //execute - same twice
        Ingredient mockIngredient = mock(Ingredient.class);
        Ingredient saved = service.save(mockIngredient);
        saved = service.save(mockIngredient);

        //verify
        assertThat(saved, is(expected));
        verify(repository, times(2)).save(mockIngredient);
    }

    //need to add fix before run it
    @Test
    @Ignore
    public void saveNewIngredient_maxLength() throws Exception {
        //setup
        Ingredient expected = new Ingredient();
        expected.setId(1L);
        expected.setName("IngredientWithNameLargerThan45CharactersLong12");
        expected.setDescription("instructions");
        when(repository.save(any(Ingredient.class))).thenReturn(expected);

        //execute - same twice
        Ingredient mockIngredient = mock(Ingredient.class);
        Ingredient saved = service.save(mockIngredient);
        saved = service.save(mockIngredient);

        //verify
        assertThat(saved, is(not(expected)));
        assertThat(saved, is(nullValue()));
        verify(repository, times(0)).save(mockIngredient);
    }

    //need to add fix before run it
   @Test
   @Ignore
    public void saveNewIngredient_emptyName() throws Exception {
        //setup
        Ingredient expected = new Ingredient();
        expected.setId(1L);
        expected.setName("");
        expected.setDescription("instructions");
        when(repository.save(any(Ingredient.class))).thenReturn(expected);

        //execute - same twice
        Ingredient mockIngredient = mock(Ingredient.class);
        Ingredient saved = service.save(mockIngredient);
        saved = service.save(mockIngredient);

        //verify
        assertThat(saved, is(not(expected)));
        assertThat(saved, is(nullValue()));
        verify(repository, times(0)).save(mockIngredient);
    }

}