package com.cookbook.app.repository;


import com.cookbook.app.config.DataSourceConfig;
import com.cookbook.app.config.PersistenceConfig;
import com.cookbook.app.domain.Ingredient;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import javax.sql.DataSource;

import java.lang.Exception;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {DataSourceConfig.class, PersistenceConfig.class})
@TestPropertySource(locations = "classpath:application-test.yml")
@Ignore
public class IngredientRepositoryTest {

    @Autowired
    private IngredientRepository repository;

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() throws Exception {
       // jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testSave() throws Exception {
       /* final Ingredient ingredient = new Ingredient();
        ingredient.setName("testSave");
        repository.save(ingredient);

        final Ingredient result = jdbcTemplate.queryForObject(
                "SELECT * FROM INGREDIENT WHERE NAME='testSave'",
                new BeanPropertyRowMapper<>(Ingredient.class));

        assertThat(result, is(notNullValue()));
        assertThat(result.getName(), is("testSave"));*/
    }

   /* @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testSaveNull() throws Exception {
        final Ingredient ingredient = null;
        repository.save(ingredient);

    }*/

//    currently doesn't fail
//    @Test(expected = Exception.class)
//    public void testSaveTwice() throws Exception {
//        final Ingredient ingredient = new Ingredient();
//        ingredient.setId(1L);
//        ingredient.setName("testSavedTwice");
//        repository.save(ingredient);
//
//        final Ingredient ingredient2 = new Ingredient();
//        ingredient2.setId(1L);
//        ingredient2.setName("testSavedTwice2");
//        repository.save(ingredient2);
//    }


    @Test
    public void testFind() throws Exception {
/*        final Ingredient ingredient = new Ingredient();
        ingredient.setName("testFind");
        repository.save(ingredient);

        final Ingredient result = jdbcTemplate.queryForObject(
                "SELECT * FROM INGREDIENT WHERE NAME='testFind'",
                new BeanPropertyRowMapper<>(Ingredient.class));

        assertThat(result, is(notNullValue()));
//        assertThat(result.getId(), is(2L));
        assertThat(result.getName(), is("testFind"));*/
    }
}